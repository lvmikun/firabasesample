const firebase = require('./firebase');
const rdb = require('./realtimeDataBase.js');


// firabaseの初期化
const fb = firebase.init();

// rdbの初期化
rdb.init(fb);

/**
 * クリック処理
 */
const testClick = () => {
    const randomid = Math.random().toString(36).slice(-8);
    rdb.set('useras/test', {id: randomid});
    rdb.get('useras/test').on('value', (data) => {
        const useridEle = document.getElementById('userid');
        useridEle.innerHTML = data.val().id;
    });
}
// 一回発火させる
testClick();

// ボタンを押したら
const clickBtn = document.getElementById('clickbtn');
clickBtn.onclick = testClick;

