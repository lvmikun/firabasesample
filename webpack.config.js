const path = require('path');
const WebpackDevServer = require('webpack-dev-server');

module.exports = {
  // エントリーポイントの設定
  entry: {
    path: path.resolve(__dirname, './src/js/app.js'),
  },
  // 出力の設定
  output: {
    // 出力先のパス（v2系以降は絶対パスを指定する必要がある）
    path: path.join(__dirname, './dist'),
    // 出力するファイル名
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'dist/'),
    port: 3000,
  },
};