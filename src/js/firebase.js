// Firebase App is always required and must be first
import firebase from "firebase";

// Add additional services that you want to use
require("firebase/auth");
require("firebase/database");
require("firebase/firestore");
require("firebase/messaging");
require("firebase/functions");

// 初期設定
// こちらはfirebaseの公式見て、設定を変更してみてください
const config = {
    
}

/**
 * Firabaseを初期化します。
 * @return {Object} firabaseインスタンス
 */
export const init = () => {
    return firebase.initializeApp(config);
}

module.init = init;