/**
 * 初期化したRealDataBaseにアクセスするインスタンス
 * NOTE:関数型の割に副作用を持つことになるので、良くない
 */
let dataBase = null;

/**
 * Firabaseデータベースのライブラリを初期化します。
 * @param {Object} firabaseインスタンス
 */
export const init = (firabase) => {
    dataBase = firabase.database();
}

/**
 * setメソッド
 * @param {string} url　set先のurl
 * @param {Object} data setするデータ
 */
export const set = (url, data) => {
    dataBase.ref(url).set(data);
}

/**
 * getメソッド
 * @param {IUserData} userData
 */
export const get = (url) => {
    return dataBase.ref(url);
}

module.init = init;
module.set  = set;
module.get  = get;